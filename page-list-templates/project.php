{container_start}

<div id="slides" class="projects" >
	<div class="slides_container">
{/container_start}
    	
		{page}
		<div class="slide">
			<div class="case">{content}</div>
		</div>
		{/page}
		
{container_end}
	</div>
	<div class="controls">
	<a class="prev arrow" href="#">previous</a> 
	<a class="next arrow" href="#">next</a> 
	</div>
</div>

{/container_end}