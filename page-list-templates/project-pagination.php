{container_start}
<script>
	jQuery(function($){
		$(".project-nav li a").hover(
			function () {
				$(this).find("span").filter(':not(:animated)').fadeTo(400, 100);
			},
			function () {
				$(this).find("span").fadeTo(200, 0);
			}
		);
	});
</script>
<div class="project-nav">
	<h3 class="widget-title">Browse Our Projects</h3>
	<div class="project-nav-wrapper">
		<ul class="pagination">
	{/container_start}
		
			{page repeat="1"}
			<li>
				<a href="#"><img alt="thumbnail" src="{image_url_project-thumbs110}"/>
					<span>
						<em>
							<em>{title}</em>
						</em>
					</span>
				</a>
			</li>
			{/page}
			
			{page}
			<li class="last">
				<a href="#"><img alt="thumbnail" src="{image_url_project-thumbs110}"/>
					<span>
						<em>
							<em>{title}</em>
						</em>
					</span>
				</a>
			</li>
			{/page}
			
	{container_end}
		</ul>
	</div>
	<div class="btn-wrapper">
		<a class="previous arrow" href="#"><span></span></a>
		<a class="next arrow" href="#"><span></span></a>
	</div>
</div>
{/container_end}