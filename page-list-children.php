<?php
/**
 * Template Name: List Children
 *
 * Creates an unordered list of the child pages of the page this template is used on.
 *
 * @package Hybrid
 * @subpackage Template
 */

get_header(); ?>

	<div class="hfeed content">

		<?php hybrid_before_content(); // Before content hook ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" class="<?php hybrid_entry_class(); ?>">

				<?php hybrid_before_entry(); // Before entry hook ?>

				<div class="entry-content">
					<?php the_content(); ?>

					<?php query_posts( array( 'post_type' => 'page', 'post_parent' => $post->ID, 'showposts' => -1, order => 'ASC' ) ); ?>

					<?php if ( have_posts() ) : ?>
						<ul>
						<?php while ( have_posts() ) : the_post(); ?>
							<li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
						<?php endwhile; ?>
						</ul>
					<?php endif; wp_reset_query(); ?>

					<?php wp_link_pages( array( 'before' => '<p class="pages">' . __('Pages:', 'hybrid'), 'after' => '</p>' ) ); ?>
				</div><!-- .entry-content -->

				<?php hybrid_after_entry(); // After entry hook ?>

			</div><!-- .hentry -->

			<?php hybrid_after_singular(); // After singular hook ?>

			<?php comments_template( '', true ); ?>

			<?php endwhile; ?>

		<?php else: ?>

			<p class="no-data">
				<?php _e('Sorry, no posts matched your criteria.', 'hybrid'); ?>
			</p><!-- .no-data -->

		<?php endif; ?>

		<?php wp_reset_query(); ?>

		<?php hybrid_after_content(); // After content hook ?>

	</div><!-- .content .hfeed -->

<?php get_footer(); ?>