<?php

add_action( 'hybrid_after_header', 'insert_project_page_jquery', 11 );
function insert_project_page_jquery(){ 

	wp_enqueue_script("mousewheel", get_bloginfo ('stylesheet_directory')."/js/jquery.mousewheel.js", array("jquery"), false, true);
	wp_enqueue_script("mwheelintent", get_bloginfo ('stylesheet_directory')."/js/mwheelIntent.js", array("jquery"), false, true);
	wp_enqueue_script("jscrollpane", get_bloginfo ('stylesheet_directory')."/js/jquery.jscrollpane.min.js", array("jquery", "mousewheel", "mwheelintent"), false, true);
	
	
	if( is_page('179') ) : ?>
	<script>
	jQuery(function($){
		$(function(){
			var pane = $('div.project-nav .project-nav-wrapper');
			pane.jScrollPane(
				{
					showArrows: true,
					animateScroll: true,
					horizontalGutter: 0,
		            verticalGutter: 0,
		            reinitialiseOnImageLoad: true
				}
			);
			var api = pane.data('jsp');
	        
			$('.project-nav a.next').bind(
                'click',
                function()
                {
                    api.scrollByY(+352);
                    return false;
                }
            ); 
		
			$('.project-nav a.previous').bind(
				'click',
				function()
				{
					api.scrollByY(-352);
					return false;
				}
			);
			
			$("#slides").slides({
				generateNextPrev: false,
				generatePagination: false,
				play: 0,
				crossfade: false,
				effect: "slide",
				slideSpeed: 800,
				slideEasing: "easeInOutQuad",
				preload: false,
				autoHeight: true,
				preloadImage: '<? bloginfo ('stylesheet_directory') ?>/images/ajax-loader-on-black.gif',
				animationComplete: function(current){
					var totalSlides = $('ul.pagination li').length;
					curSlide = current-1;
					//console.log('cur slide: ', curSlide);
					//console.log('total slides: ', totalSlides);
					if(current == totalSlides){
						var currentThumb = $('a[href$="'+totalSlides-1+'"]:first');
					} else {
						var currentThumb = $('a[href$="'+curSlide+'"]:first');
					}
					api.scrollToElement(currentThumb, true);					
				}
			}); 
		});
		
	});
	
	</script>
	<?php
	endif;
}

add_action( 'hybrid_after_header', 'insert_jquery_slideshows', 11 );
function insert_jquery_slideshows(){ 
	if( is_front_page() ) : ?>
	<script>
	jQuery(function($){
		$("#slides").slides({
			generateNextPrev: false,
			generatePagination: false,
			fadeSpeed: 800,
			play: 6000,
			crossfade: true,
			effect: "fade",
			fadeEasing: "easeInOutQuad",
			preload: false,
			preloadImage: '<? bloginfo ('stylesheet_directory') ?>/images/ajax-loader-on-black.gif'
		}); 
	});
	</script>
	<div id="movie-wrapper">
		<div id="movie-inner">
			<div class='careers-photo'>
				<img src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/careers.jpg' alt='Careers photo'/>
				<span>Find your Passion, Build Your Career With NV5</span>
			</div>
			<div id="slides">
				<div class="slides_container">
					<div class="slide slide1">
						<img class='welcome1' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/welcome-1.jpg' alt='Welcome photo'/>
						<span>Citizen's Bank Park, Philadelphia, PA</span>
						<a href="<? echo get_permalink(179); ?>">Past experience is a key indication of future success</a>
						<em>Photograph courtesy of Matt Wargo</em>
					</div>
					<div class="slide slide2">
						<img class='welcome2' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/welcome-2.jpg' alt='Welcome photo'/>
						<span>Gaslamp Quarter, San Diego, CA</span>
						<a href="<? echo get_permalink(179); ?>">See more examples of our work</a>
					</div>
					<div class="slide slide3">
						<img class='welcome3' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/welcome-3.jpg' alt='Welcome photo'/>
						<span>Reno ReTRAC, Reno, Nevada</span>
						<a href="<? echo get_permalink(179); ?>">See more examples of our work</a>
					</div>
					<div class="slide slide4">
						<img class='welcome4' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/welcome-4.jpg' alt='Welcome photo'/>
						<span>US Federal Courthouse, San Diego, CA</span>
						<a href="<? echo get_permalink(179); ?>">See more examples of our work</a>
					</div>
					
					<div class="slide slide5">
						<img class='welcome5' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/welcome-5.jpg' alt='Welcome photo'/>
						<span>Lincoln Financial Field, Philadelphia, PA</span>
						<a href="<? echo get_permalink(179); ?>">See more examples of our work</a>
					</div>
					<div class="slide slide6">
						<img class='welcome6' src='<? bloginfo ('stylesheet_directory') ?>/images/welcome/careers.jpg' alt='Careers photo'/>
						<span>Find your Passion</span>
						<a href="<? echo get_permalink(181); ?>">Build Your Career With NV5</a>
					</div>
				</div>
			</div>
			<div id="twitter-wrapper">
				<h2>News Feed</h2>
				<? twitter_messages('nv5_inc',4,true,true,false,true,false);?>
				<? /* twitter_messages('username', [msgs], [list], [timestamp], ['link'], [hyperlinks], [twitter_users], [encode_utf8]); */ ?>
				
				
				<!-- <p>Art exhibit by one of Nolte’s own -William Ishmael! <a href="http://t.co/ZeVVTiT8">http://rosevillept.com/detail/187983.html</a><em>20 Sep</em>
				</p>
				<hr/>
				<p>NV5 August Sustainable Design News <a href="http://t.co/dlsJKla">http://icont.ac/brs4</a><em>18 Aug</em>
				</p>
				<hr/>
				<p>Elbow Space: Our Palm Desert Office Has Moved! Stop by any time & we'll happily show you our (expanded) elbow space. <a href="http://icont.ac/1Gay">http://icont.ac/1Gay</a><em>5 Jul</em>
				</p>
				<hr/>
				<p><a href="http://twitpic.com/5fajpv">http://twitpic.com/5fajpv</a><em>22 Jun</em></p>
				<hr/>
				<p>NV5 Ranked 158 by ENR Top 500 Design Firms Survey <a href="http://t.co/kChlRxZ">http://enr.construction.com/toplists/DesignFirms/101-200.asp</a><em>26 Apr</em></p> -->
			</div>
		</div>
	</div>
	<?php
	endif;
}

add_shortcode('vertical_hover_component', 'verticals_shortcode');
 
function verticals_shortcode(){
	ob_start();
	?>
	<script>
	jQuery(document).ready(function($){
		$('.home div.box').hover(
		  function () {
		    $(this).find('p').clearQueue().animate({
		      marginTop: '-115px'
		    })
		  },
		  function () {
		    $(this).find('p').clearQueue().animate({
		      marginTop: '0px'
		    })
		  }
		);
	});
	</script>
	<div class="box">
		<p>
			<a href="<? echo site_url() ?>/about-nv5/services/construction-quality-assurance/"><img width="172" height="110" alt="" src="<? echo site_url() ?>/wp-content/uploads/construction-quality-assurance.jpg" class="alignnone size-full wp-image-243">
			<strong>Construction &nbsp;Quality<br>
			Assurance</strong>
			<span><em>At NV5, construction quality assurance starts before construction begins.</em></span></a>
		</p>
	</div>
	<div class="box">
		<p>
			<a href="<? echo site_url() ?>/about-nv5/services/infrastructure-engineering/"><img width="172" height="110" alt="" src="<? echo site_url() ?>/wp-content/uploads/infrastructure-engineering.jpg" class="alignnone size-full">
			<strong>Infrastructure<br>
			Engineering</strong>
			<span><em>NV5 has over 60 years of experience designing the infrastructure that supports community development.</em></span></a>
		</p>
	</div>
	<div class="box">
		<p>
			<a href="<? echo site_url() ?>/about-nv5/services/municipal-outsourcing/"><img width="172" height="110" alt="" src="<? echo site_url() ?>/wp-content/uploads/municipal-outsourcing.jpg" class="alignnone size-full">
			<strong>Municipal<br>
			Outsourcing</strong>
			<span><em>With growing budget deficits, our local government clients are being increasingly constrained in their ability to deliver services to their community.</em></span></a>
		</p>
	</div>
	<div class="box">
		<p>
			<a href="<? echo site_url() ?>/about-nv5/services/asset-management/"><img width="172" height="110" alt="" src="<? echo site_url() ?>/wp-content/uploads/asset-management.jpg" class="alignnone size-full">
			<strong>Asset<br>
			Management</strong>
			<span><em>When it comes to asset management, NV5 helps our clients see the entire picture.</em></span></a>
		</p>
	</div>
	<div class="box">
		<p>
			<a href="<? echo site_url() ?>/about-nv5/services/environmental-services/"><img width="172" height="110" alt="" src="<? echo site_url() ?>/wp-content/uploads/environmental-services.jpg" class="alignnone size-full">
			<strong>Environmental<br>
			Services</strong>
			<span><em>At NV5, we focus on reducing liability exposure as well as improving health and safety in the workplace.</em></span></a>
		</p>
	</div>
	<?
	$ob_str=ob_get_contents();
	ob_end_clean();
	return $ob_str;
}

?>