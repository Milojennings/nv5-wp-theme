<?php
/**
 * Footer Template
 *
 * The footer template is generally used on every page of your site. Nearly all other
 * templates call it somewhere near the bottom of the file. It is used mostly as a closing
 * wrapper, which is opened with the header.php file. It also executes key functions needed
 * by the theme, child themes, and plugins. 
 *
 * @package Hybrid
 * @subpackage Template
 */
?>
		<?php do_atomic('after_container'); // After container hook ?>


</div><!-- #body-container --> 

<div id="footer-container">

	<div id="footer">
	
		<?php do_atomic('before_footer'); // Before footer hook ?>
		
		<?php do_atomic('footer'); // Hybrid footer hook ?>

	</div><!-- #footer -->

	<?php do_atomic('after_footer'); // After footer hook ?>

</div><!-- #footer-container -->



<?php wp_footer(); // WordPress footer hook ?>
<?php do_atomic('after_html'); // After HTML hook ?>

<img class="cityscape" alt="cityscape" src="<?php echo get_bloginfo('stylesheet_directory') ?>/images/cityscape-bg.png" />
</body>
</html>