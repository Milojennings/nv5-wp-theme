<?php
/**
 * Functions File
 *
 * This is your child theme's functions.php file. Use this to write any custom functions 
 * you may need. You can use this to hook into the Hybrid theme (could also create a 
 * plugin to do the same).
 *
 * @package Skeleton
 * @subpackage Functions
 */

add_action( 'after_setup_theme', 'my_child_theme_setup', 11 );

function my_child_theme_setup()
{
	/* Get the theme prefix ("hybrid"). */
	$prefix = hybrid_get_prefix();
	
	//Replace instances of the word "response" with "comment" on comment forms
	add_filter( 'gettext', 'my_translations', 10, 3 );
	function my_translations( $translation, $text, $domain ) {

		$translations = &get_translations_for_domain( $domain );
		if ( 'Leave a response' == $text )
			$translation = $translations->translate( 'Leave a comment' );
		if ( '%1$s Response' == $text )
			$translation = $translations->translate( '%1$s comment' );
		if ( 'One response to %1$s' == $text )
			$translation = $translations->translate( 'One comment' );
		if ( '%1$s Responses' == $text )
			$translation = $translations->translate( '%1$s comments' );
		if ( '%1$s responses to %2$s' == $text )
			$translation = $translations->translate( '%1$s comments' );
		if ( 'No responses to %1$s' == $text )
			$translation = $translations->translate( 'No comments' );
		if ( 'Leave a Reply' == $text )
			$translation = $translations->translate( 'Leave a comment' );
		return $translation;
	}
	
	
	//Disable built-in Hybrid SEO fields. We will use another plugin instead
	remove_theme_support( 'hybrid-core-seo' );
	function my_disabler( $var ) {
		return '';
	}

	//Remove Bread crumbs
	add_action('init', 'remove_breadcrumb_trail');
	function remove_breadcrumb_trail() {
		remove_action('hybrid_before_content', 'hybrid_breadcrumb');
	}
	
	//Move primary menu into header container
	remove_action( 'hybrid_after_header', 'hybrid_get_primary_menu' );
	add_action( 'hybrid_header', 'hybrid_get_primary_menu', 11 );
	
	//Disable site title H1 and site description
	add_filter( 'hybrid_site_title', 'my_disabler' );
	add_filter( 'hybrid_site_description', 'my_disabler' );

	//Remove some of the default meta info
	add_filter( "hybrid_meta_author", '__return_false' );
	add_filter( "hybrid_meta_template", '__return_false' );
	add_filter( "hybrid_meta_copyright", '__return_false' );

	// Remove Author and add date to byline on 
	// blog home page, single blog entries, search results, archives & category pages	
	add_action( 'template_redirect', 'adjust_bylines' );
	function adjust_bylines() {
			
		add_filter( "hybrid_byline", "remove_author_from_byline");
		function remove_author_from_byline( $byline ) {
			if(is_single()){
				//$byline = '<p class="byline" style="margin-bottom:10px;">' . get_the_time('F Y') . '</p>';
				$byline = '';
			} elseif( is_home() | is_search() | is_archive() ){
				$byline = '<div class="time-wrapper"><div class="month">'. get_the_time("M") .'</div><div class="year">'. get_the_time("Y") .'</div></div>';
				//$byline = '<p class="byline"><abbr title="' . get_the_time('m/j/y g:i A') . '">' . get_the_time('F Y') . '</abbr>[entry-edit-link before=" "]</p>';
				//$byline = '';
			}
			return $byline;
				
			/* example code to reposition byline */
			//remove_action( 'hybrid_before_entry', 'hybrid_byline' );
			//add_action( 'hybrid_after_entry', 'hybrid_byline' );
		}
	}
	
	add_action( 'template_redirect', 'adjust_entry_meta' );
	function adjust_entry_meta(){
		add_filter( 'hybrid_entry_meta', 'filter_entry_meta' );
		function filter_entry_meta($meta){
			if( is_single() ){
				$meta = '<div id="entry-meta"><p class="entry-meta">' . get_the_time('F Y') . ' in [entry-terms taxonomy="category"]</p></div>';
			} elseif ( is_home() | is_single() | is_search() | is_archive() ) {
				$meta = '<div id="entry-meta"><p class="entry-meta">Posted in [entry-terms taxonomy="category"]</p></div>';
			}
			//$meta = '<div id="entry-meta"><p class="entry-meta">' . get_the_time('F Y') . ' in [entry-terms taxonomy="category"]</p></div>';
			
			return $meta;
		}
		
	}
	
	
	
	
	//Remove commenting functionality from pages
	add_filter( 'comments_template', 'remove_comments_template', 11 );
	function remove_comments_template( $file ) {
		if ( is_page() )
			$file = STYLESHEETPATH . '/comments-page.php';
		return $file;
	}

	//Add Smart 404 suggestions to output of 404 page if plugin is installed
	add_action('hybrid_after_content','add_smart404_output');
	function add_smart404_output(){
		if( is_404() ){
			if( function_exists('smart404_suggestions') ){?>
				<div class="smart404_suggestions_container">
					<h2>Suggestions</h2>
					<?php smart404_suggestions(); ?>
				</div>
			<?}
		}
	}
	
	//Replace default superfish dropdown script
	add_filter( 'hybrid_drop_downs_script', 'replace_drop_down_script' );
	function replace_drop_down_script( $script ) {
		return get_bloginfo('stylesheet_directory') . '/js/drop-downs.js';
	}

	// Point favicon to theme_folder/images/favicon.ico
	add_action('wp_head', 'blog_favicon');
	function blog_favicon() {
		echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.ico" />' . "\n";
		echo "<link href='http://fonts.googleapis.com/css?family=Kreon:300,400' rel='stylesheet' type='text/css'>";
	}
	
	add_action( 'hybrid_after_header', 'service_panels', 11 );
	function service_panels(){ 
		if( is_front_page() ) : ?>
		
		<script>
		jQuery(document).ready(function($){
			$('.home div.box').hover(
			  function () {
			    $(this).find('p').clearQueue().animate({
			      marginTop: '-115px'
			    })
			  },
			  function () {
			    $(this).find('p').clearQueue().animate({
			      marginTop: '0px'
			    })
			  }
			);
		});
		</script>
	<? endif;
	}		
	
	// Wrap menu, logo and header in a div
	add_action('hybrid_before_header','child_theme_add_menu_wrapper_div', 12);
	function child_theme_add_menu_wrapper_div(){?>
		<div id="header-wrapper">
	<?}
	// Close header wrapper div
	add_action('hybrid_after_header','child_theme_close_menu_wrapper_div');
	function child_theme_close_menu_wrapper_div(){?>
		</div>
	<?}
	
	// Add inner wrapper div
	add_action('hybrid_before_container','child_theme_add_inner_div');
	function child_theme_add_inner_div(){
		echo '<div id="inner">';
	}
	add_action('hybrid_after_container','child_theme_inner_div',100);
	function child_theme_inner_div(){
		echo '</div>';
	}
	add_action('hybrid_after_container','child_theme_social_rail',100);
	function child_theme_social_rail(){
		echo '</div>';
		?>
		<div id="social-wrapper">
			<a target="_blank" href="<? get_cinfotag("facebook-url"); ?>"><span class="icon-facebook"></span>Facebook</a>
			<a target="_blank" href="<? get_cinfotag("twitter-url"); ?>"><span class="icon-twitter"></span>Twitter</a>
			<a target="_blank" href="<? get_cinfotag("linkedin-url"); ?>"><span class="icon-linkedin"></span>LinkedIn</a>
			<!--<a target="_blank" href="<? get_cinfotag("youtube-url"); ?>"><span class="icon-youtube"></span>Youtube</a>-->
			<a target="_blank" href="<? get_cinfotag("vimeo-url"); ?>"><span class="icon-vimeo"></span>Vimeo</a>
			<a target="_blank" href="<? echo get_permalink(189); ?>"><span class="icon-blog"></span>Blog</a>
			<span style="float: right;">© NV5 2012. All Rights Reserved</span>
		</div>
		<?
	}
	
	
	// add class "blog" to archives, categories and search results
	// add IE classes to body
	add_filter( 'hybrid_body_class', 'add_body_classes' );
	function add_body_classes( $class ) {
		global $wp_query;
		if( is_page_template('page-blog.php') || is_archive() || is_search() ){
			$class .= ' blog';
		}
		$browser = $_SERVER[ 'HTTP_USER_AGENT' ];
		if ( preg_match( "/MSIE 6.0/", $browser ) )
			$class .= ' ie6';
		elseif ( preg_match( "/MSIE 7.0/", $browser ) )
			$class .= ' ie7';
		elseif ( preg_match( "/MSIE 8.0/", $browser ) )
			$class .= ' ie8';
		elseif ( preg_match( "/MSIE 9.0/", $browser ) )
			$class .= ' ie9';
		return $class;
	}
	
	// Set default thumbnail that appears next to posts
	add_filter('get_the_image_args', 'get_the_image_args_new');
	function get_the_image_args_new() {
		//'default_image' => get_bloginfo ('stylesheet_directory') . '/images/default-post-thumbnail.jpg', 
		return array(	
			'size' => 'postthumbnail',
			'image_scan' => true
			
		);
	};
	
	//Make excerpt readmore string a link
	function new_excerpt_more($more) {
		global $post;
		return '… <a class="more-link" href="'. get_permalink($post->ID) . '">' . 'Read More' . '</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
	
	/**
	 * Start custom code
	 */
	
	// Add site title as logo
	add_action('hybrid_header', 'site_logo', 10);
	function site_logo() {?>
		<div id="logo-wrapper">
			<a id="logo" href="<?php echo site_url() ?>"><img alt="<? get_cinfotag("logo-alt"); ?>" src="<? get_cinfotag("logo-url"); ?>"></a>
		</div>
	<?}
	
	add_action('hybrid_header', 'site_search', 12);
	function site_search() {?>
		<div id="extra-button-wrapper">
			<a href="<? echo get_permalink(177); ?>"><span class="icon-globe"></span>Locations</a>
			<a href="<? echo get_permalink(106); ?>"><span class="icon-chat"></span>Contact Us</a>
		</div>
		<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
			<div>
				<input class="search-text" type="text" name="s" id="search-text-1" value="Search" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
				<input type="submit" id="searchsubmit" value="" class="btn" />
			</div>
		</form>
	<?}
	
	
	// REMOVES LINKS FROM H1 HEADINGS
	add_filter( 'hybrid_entry_title', 'remove_links_from_headings' );
	function remove_links_from_headings($title){
		if ( is_front_page() || is_page() ) {
			return '<h1 class="page-title entry-title">' . get_the_title() . '</h1>';
		} else if ( is_single() ) {
			return '<h1 class="post-title entry-title">' . get_the_title() . '</h1>';
		} else {
			return $title;
		}
	}

	//Remove widget areas 
	//Use conditional tags to decide where they should and shouldn't show up
	
	add_filter( 'sidebars_widgets', 'disable_widget_areas' );
	function disable_widget_areas( $sidebars_widgets ) {
		if( is_front_page() ){
			$sidebars_widgets['primary'] = false;
			$sidebars_widgets['secondary'] = false;
			$sidebars_widgets['subsidiary'] = false;
			$sidebars_widgets['before-content'] = false;
			$sidebars_widgets['after-content'] = false;
			$sidebars_widgets['after-singular'] = false;
		}
		return $sidebars_widgets;
	}
	
	
	//Enque a javascript file when not viewing site in admin interface	
	if ( !is_admin() ) { 
		wp_enqueue_script("jquery-easing", get_bloginfo ('stylesheet_directory')."/js/jquery.easing.1.3.min.js", array("jquery"), false, true);
		wp_enqueue_script("slidesJS", get_bloginfo ('stylesheet_directory')."/js/slides.jquery.js", array("jquery"), false, true);
	}
	
	include 'jquery-slideshows.php';
	
	add_filter('get_the_image', 'no_image_for_stickies');
	function no_image_for_stickies($array) {
		global $post;
		if ( is_sticky( $post->ID ) ){
			return false;
		} else {
			return $array;
		}
	}
	add_filter( 'the_excerpt' , 'full_content_for_stickies');
	function full_content_for_stickies($content) {
		global $post;
		if ( is_sticky( $post->ID ) ){
			$content = $post->post_content;
			return $content;
		}
		return $content;
	}
	
}

add_shortcode('nv5_map', 'nv5_map_shortcode');

function nv5_map_shortcode(){
	ob_start();
	?>
	<div id="locations-wrapper">
		<div id="locations">
		<iframe src="http://batchgeo.com/map/177f59d5128d1d1b1d10ab1eccd64d87" frameborder="0" width="100%" height="615"></iframe>
		</div>
	</div>
	<?
	$ob_str=ob_get_contents();
	ob_end_clean();
	return $ob_str;
}

add_action('wp_footer', 'news_feed_links_js');
function news_feed_links_js(){?>
	<script type="text/javascript">
		//make all links to PDFs external
		jQuery(document).ready(function(){
		 jQuery(".twitter a").attr('target','_blank');
		});
	</script>	
<?}